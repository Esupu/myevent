package com.eshael.myevents;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.eshael.myevents.model.Response;
import com.eshael.myevents.model._Response;
import com.eshael.myevents.util.ConnectionClass;
import com.eshael.myevents.util.Network;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class SignUp extends AppCompatActivity implements Network , View.OnClickListener{

    private EditText username;
    private EditText email;
    private EditText password;
    private EditText conf_password;
    private EditText phone;
    private ProgressDialog progressDialog;
    private Gson gson;
    private Button signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        try
        {
            getSupportActionBar().hide();
        }catch (NullPointerException e){}

        username = (EditText)findViewById(R.id.reg_username);
        email  = (EditText)findViewById(R.id.reg_email);
        password = (EditText)findViewById(R.id.reg_password);
        conf_password = (EditText)findViewById(R.id.conf_reg_password);
        phone = (EditText)findViewById(R.id.reg_phone);
        signup = (Button)findViewById(R.id.reg_btn);
        signup.setOnClickListener(this);
        progressDialog = new ProgressDialog(this);
        gson = new Gson();

    }


    private void createUser(String username, String password, String email,String phone)
    {
        progressDialog.setMessage("Please hold as your account is created");
        progressDialog.setTitle("MyEvents SignUp");
        progressDialog.show();

        String[] paramenters = {"username","password","email","phone"};
        String[] values = {username,password,email,phone};
        ConnectionClass.ConnectionClass(SignUp.this, SIGN_UP, paramenters, values, " ",
                new ConnectionClass.VolleyCallback()
                {
                    @Override
                    public void onSuccess(String serverstring)
                    {
                        Log.d("Response:", serverstring.toString());

                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(serverstring).getAsJsonObject();

                        _Response response = gson.fromJson(obj, _Response.class);
//
                        Log.d("Response:", response.getMessage());
                        if(response.getMessage().equalsIgnoreCase("Success!"))
                        {
                            progressDialog.cancel();
                            startActivity(new Intent(SignUp.this,LoginActivity.class));
                            finish();
                            Toast.makeText(SignUp.this,"Signup Successfull",Toast.LENGTH_LONG).show();
                        }
//                        else
//                        {
//                            progressDialog.cancel();
//
//                            Toast.makeText(SignUp.this,"Login failed\nPlease try again",Toast.LENGTH_LONG).show();
//                        }

                    }
                });
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id)
        {
            case R.id.reg_btn:


                if(password.getText().toString().equals(conf_password.getText().toString()))
                {
                    createUser(username.getText().toString().trim(),password.getText().toString().trim(),email.getText().toString().trim(),phone.getText().toString().trim());
                }
                else
                {
                    Toast.makeText(SignUp.this, "Passwords don't match", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }
}
