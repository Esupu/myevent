package com.eshael.myevents.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.eshael.myevents.R;
import com.eshael.myevents.adapter.EventsAdapter;
import com.eshael.myevents.model.Event;
import com.eshael.myevents.util.AppController;
import com.eshael.myevents.util.Network;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raphael on 6/11/2016.
 */
public class CategoryEvent extends Fragment implements Network{

    private View rootView;
    private List<Event> eventList = new ArrayList<>();
    private EventsAdapter adapter;
    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.event_fragment,container,false);

        listView = (ListView)rootView.findViewById(R.id.eventList);
        adapter = new EventsAdapter(getContext(),eventList);

        listView.setAdapter(adapter);
        getEvents();

        return rootView;
    }

    private void getEvents()
    {
        JsonArrayRequest request = new JsonArrayRequest(CATEGORY_EVENT+getArguments().getString("category"), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {

                try
                {
                    for (int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Event event = new Event();

                        event.setId(jsonObject.getInt("id"));
                        event.setName(jsonObject.getString("event_name"));
                        event.setThumbnail(jsonObject.getString("thumbnail"));
                        event.setPromoter(jsonObject.getString("promoter"));
                        event.setStart_time(jsonObject.getString("start_time"));
                        event.setDescription(jsonObject.getString("description"));
                        event.setUser_promoter(jsonObject.getString("user_promoter"));

                        eventList.add(event);
                    }
                    adapter.notifyDataSetChanged();

                }catch (JSONException e)
                {
                    System.out.println("Error : " + e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });

        AppController.getInstance().addToRequestQueue(request);
    }

}
