package com.eshael.myevents.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.eshael.myevents.R;
import com.eshael.myevents.adapter.CategoryAdapter;
import com.eshael.myevents.model.Category;
import com.eshael.myevents.util.AppController;
import com.eshael.myevents.util.Network;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raphael on 6/11/2016.
 */
public class CategoriesFragment extends Fragment implements Network{

    private View rootView;
    private List<Category> categoryList = new ArrayList<>();
    private ListView listView;
    private CategoryAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.categories_fragment,container,false);
        listView = (ListView)rootView.findViewById(R.id.list_categories);
        adapter = new CategoryAdapter(getContext(),categoryList);
        listView.setAdapter(adapter);

        getCategories();

        return rootView;
    }

    private void getCategories()
    {
        JsonArrayRequest request = new JsonArrayRequest(CATEGORIES, new Response.Listener<JSONArray>()
        {

            @Override
            public void onResponse(JSONArray jsonArray) {
                Log.d("Categories",jsonArray+"");
                try
                {
                    for (int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Category category = new Category();
                        category.setEvent_category(jsonObject.getString("event_category"));
                        categoryList.add(category);
                    }

                }catch (JSONException e)
                {

                }

                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });

        AppController.getInstance().addToRequestQueue(request);
    }

}
