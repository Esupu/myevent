package com.eshael.myevents.model;

/**
 * Created by Raphael on 6/11/2016.
 */
public class Ticket {

    private String eevnt_id;
    private String username;
    private String event_title;
    private String amount;
    private String code;
    private String user_promoter;
    private String status;
    private String ticket_category;

    public Ticket() {
    }

    public String getEevnt_id() {
        return eevnt_id;
    }

    public void setEevnt_id(String eevnt_id) {
        this.eevnt_id = eevnt_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEvent_title() {
        return event_title;
    }

    public void setEvent_title(String event_title) {
        this.event_title = event_title;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUser_promoter() {
        return user_promoter;
    }

    public void setUser_promoter(String user_promoter) {
        this.user_promoter = user_promoter;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTicket_category() {
        return ticket_category;
    }

    public void setTicket_category(String ticket_category) {
        this.ticket_category = ticket_category;
    }
}
