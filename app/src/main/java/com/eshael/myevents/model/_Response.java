package com.eshael.myevents.model;

/**
 * Created by Raphael on 6/11/2016.
 */
public class _Response {

    private String message;
    private String result;

    public _Response() {
    }

    public _Response(String message, String result) {
        this.message = message;
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
