package com.eshael.myevents.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.eshael.myevents.R;
import com.eshael.myevents.VerifyQR;
import com.eshael.myevents.model.Ticket;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raphael on 6/11/2016.
 */
public class TicketAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<Ticket> ticketList = new ArrayList<>();
    private Context context;


    public TicketAdapter(Context context, List<Ticket> ticketList) {
        this.context = context;
        this.ticketList = ticketList;
    }

    @Override
    public int getCount() {
        return ticketList.size();
    }

    @Override
    public Object getItem(int position) {
        return ticketList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(layoutInflater == null)
            layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null)
            convertView = layoutInflater.inflate(R.layout.ticket_row,null);

        Ticket ticket = ticketList.get(position);

        TextView tittle = (TextView) convertView.findViewById(R.id.t_title);
        TextView category = (TextView) convertView.findViewById(R.id.t_category);
        TextView amount = (TextView) convertView.findViewById(R.id.amount);
        final TextView code = (TextView) convertView.findViewById(R.id.code);
        Button verify = (Button)convertView.findViewById(R.id.verify_ticket);

        tittle.setText("Title : "+ticket.getEvent_title());
        category.setText("Category : "+ticket.getTicket_category());
        amount.setText("Amount : "+ticket.getAmount());
        code.setText("Code : "+ticket.getCode());

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, VerifyQR.class));
            }
        });

        return convertView;
    }
}
