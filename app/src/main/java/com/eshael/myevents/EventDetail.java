package com.eshael.myevents;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.eshael.myevents.fragments.TicketsFragment;
import com.eshael.myevents.model.Price;
import com.eshael.myevents.util.AppController;
import com.eshael.myevents.util.ConnectionClass;
import com.eshael.myevents.util.Network;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.StringTokenizer;

public class EventDetail extends AppCompatActivity implements Network, View.OnClickListener {

    private ImageView imageView;
    private Price price = new Price();
    private TextView desc,location;
    private Button single,couple,silver,cooperate;
    LinearLayout prices;
    Intent intent;
    private String MyPREFERENCES;
    private SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        intent = getIntent();
        setContentView(R.layout.activity_event_detail);

        try
        {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(intent.getStringExtra("title"));
        }catch (NullPointerException e)
        {

        }

        desc = (TextView) findViewById(R.id.desc);
        prices = (LinearLayout)findViewById(R.id.prices);
        single = (Button)findViewById(R.id.single);
        couple = (Button)findViewById(R.id.couple);
        silver = (Button)findViewById(R.id.silver);
        cooperate = (Button)findViewById(R.id.cooperate);
        location = (TextView)findViewById(R.id.location);

        location.setOnClickListener(this);
        single.setOnClickListener(this);
        couple.setOnClickListener(this);
        silver.setOnClickListener(this);
        cooperate.setOnClickListener(this);

        imageView = (ImageView)findViewById(R.id.detail_event_image);
        Picasso.with(EventDetail.this).load(BASE_THUMB+intent.getStringExtra("thumb")).into(imageView);
        getPrices(intent.getIntExtra("id",1));

        desc.setText(intent.getStringExtra("desc"));
        location.setText("View Map");

    }


    private void getPrices(int id)
    {
        JsonObjectRequest request  = new JsonObjectRequest(Request.Method.GET,EVENT+id+"/", new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject)
            {
                Log.d("EVENT " ,jsonObject+"");
                try
                {
                    price.setSingle(jsonObject.getDouble("single"));
                    price.setCouple(jsonObject.getDouble("couple"));
                    price.setCorporate(jsonObject.getDouble("corporate"));
                    price.setSilver(jsonObject.getDouble("silver"));
                    price.setId(jsonObject.getString("event"));

                    System.out.println("Single : " + jsonObject.getDouble("single"));
                    prices.setVisibility(View.VISIBLE);
                    single.setText("Single \n"+jsonObject.getDouble("single")+"/=");
                    couple.setText("Couple\n"+price.getCouple().toString()+"/=");
                    silver.setText("Silver\n"+price.getSilver().toString()+"/=");
                    cooperate.setText("Cooperate\n"+price.getCorporate().toString()+"/=");

                }catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id)
        {
            case R.id.single:
                    placeBooking(price.getId(),intent.getStringExtra("title"),price.getSingle(),"single",randomString(),intent.getStringExtra("prom"));
                break;
            case R.id.couple:
                placeBooking(price.getId(),intent.getStringExtra("title"),price.getCouple(),"couple",randomString(),intent.getStringExtra("prom"));
                break;
            case R.id.silver:
                placeBooking(price.getId(),intent.getStringExtra("title"),price.getSilver(),"silver",randomString(),intent.getStringExtra("prom"));
                break;
            case R.id.cooperate:
                placeBooking(price.getId(),intent.getStringExtra("title"),price.getCorporate(),"cooperate",randomString(),intent.getStringExtra("prom"));
                break;
            case R.id.location:

                String [] subStr = intent.getStringExtra("loc").trim().split(",");

                String latitude = subStr[0];
                String longitude = subStr[1];

                String uri = String.format(Locale.ENGLISH, "geo:"+latitude+","+ longitude);

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
                break;

        }
    }

    private void placeBooking(String  event_id,String title,Double amount,String type,String code,String promoter)
    {
        //Username, id, promoter, event title, amount ,category, generated code

        System.out.println(type + "picked");
        alerterSuccessSimple("Book " + type,amount+"",event_id,title,amount.toString(),type,code,promoter);
    }

    public void alerterSuccessSimple(String header, String message,final String  event_id, final String title, final String amount, final String type, final String code, final String promoter)
    {

        final String header2 = header;

        LayoutInflater li = LayoutInflater.from(this);
        View promptsView;
        promptsView = li.inflate(R.layout.dialog_success_simple, null);

        TextView headerTxt = (TextView) promptsView.findViewById(R.id.success_simple_header);
        TextView messageTxt = (TextView) promptsView.findViewById(R.id.success_simple_message);

        headerTxt.setText(header);
        messageTxt.setText("Transfer " + message);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setView(promptsView);
        // set dialog message
        alertDialogBuilder
                .setCancelable(true)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                String[] paramenters = {"username", "event_id", "user_promoter", "event_title", "amount" ,"ticket_category", "code"};
                                String[] values = {sharedpreferences.getString("username",""),event_id,promoter,title,amount,type,code};
                                ConnectionClass.ConnectionClass(EventDetail.this, BASE_URL, paramenters, values, " ",
                                        new ConnectionClass.VolleyCallback()
                                        {
                                            @Override
                                            public void onSuccess(String serverstring)
                                            {
                                                Log.d("Response:", serverstring.toString());

                                                Toast.makeText(EventDetail.this,"Event booked.. ",Toast.LENGTH_LONG).show();

//                                                JsonParser parser = new JsonParser();
//                                                JsonObject obj = parser.parse(serverstring).getAsJsonObject();
                                            }
                                        });
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    private String  randomString()
    {
        return RandomStringUtils.randomAlphanumeric(5).toUpperCase();
    }

}
