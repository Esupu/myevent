package com.eshael.myevents.util;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Raphael on 11/18/2015.
 */
public class Connection {


    static String finalstring;
    static Gson gson;

    public static String Connection(final Context context,String URL_FEED, String json, final String tag, final VolleyCallback callback) {

        gson = new Gson();
        JSONObject jsonObject;

        System.out.println("JSON : " + json);
        try {

            jsonObject = new JSONObject(json);

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST,URL_FEED, jsonObject,
                new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                try {
                    VolleyLog.v("Response:", response.toString(4));
                    if (response != null) {
                        Log.d("respo==we have==",response.toString());
                        finalstring = response.toString();
                        callback.onSuccess(finalstring);

                        AppController.getInstance().cancelPendingRequests(tag);
                    } else {

                        Log.d("null==",response.toString());

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                params.put("charset", "utf-8");

                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(postRequest);

        }catch (Exception e){}
        return URL_FEED;
    }

        public interface VolleyCallback{
            void onSuccess(String result);
        }
}
